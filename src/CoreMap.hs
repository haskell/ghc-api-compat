module CoreMap
  ( module GHC.Core.Map.Expr
  , module GHC.Core.Map.Type
  )
where

import GHC.Core.Map.Expr
import GHC.Core.Map.Type
