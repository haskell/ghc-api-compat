{-# LANGUAGE CPP #-}

module Outputable
    ( module GHC.Utils.Outputable
#if !MIN_VERSION_GLASGOW_HASKELL(9,0,0,0)
    -- now exported again from Outputable
    , initSDocContext
#endif
    ) where

import GHC.Utils.Outputable
#if !MIN_VERSION_GLASGOW_HASKELL(9,0,0,0)
import GHC.Driver.Session (initSDocContext)
#endif
