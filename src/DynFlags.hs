{-# LANGUAGE CPP #-}
module DynFlags
  ( module GHC.Driver.Session,
    Way(..),
    wayRTSOnly,
    wayGeneralFlags,
    wayUnsetGeneralFlags,
    buildTag,
    dynamicGhc,
    hostFullWays,
    interpWays,
  )
where

import Data.Set (Set)
import GHC.Driver.Session
#if MIN_VERSION_GLASGOW_HASKELL(9,2,0,0)
import GHC.Platform.Ways
#else
import GHC.Driver.Ways
#endif

buildTag :: DynFlags -> String
buildTag = waysBuildTag . ways

dynamicGhc :: Bool
dynamicGhc = hostIsDynamic

interpWays :: Set Way
interpWays = hostFullWays
